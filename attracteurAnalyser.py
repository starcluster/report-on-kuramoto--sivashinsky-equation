from math import *;
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from mpl_toolkits.mplot3d import Axes3D
import warnings


azerty = pi/180;
def calculateBarycenter(attracter):
    bc = [attracter[0][0], attracter[0][2], attracter[0][2]];
    for i in range(0, len(attracter)):
        bc[0] += attracter[i][0];
        bc[1] += attracter[i][1];
        bc[2] += attracter[i][2];
    N = len(attracter);
    bc[0]/=N;
    bc[1]/=N;
    bc[2]/=N;
    return bc;

def dot(u, v):
    return u[0]*v[0] + u[1]*v[1] + u[1]*v[1];

def support(attracter, u):
    mdot = dot(attracter[0], u);
    mx = attracter[0];
    for i in range(1, len(attracter)):
        odot = dot(attracter[i], u);
        if(odot > mdot):
            mdot = odot;
            mx = attracter[i];
    return [mdot, mx];

def sphereUnit(r, a, b):
    x = cos(a)*cos(b)*r;
    y = sin(a)*cos(b)*r;
    z = sin(b)*r;
    return [x,y,z];

def calculateRadius(attracter, d, bc):
    da = d[0];
    db = d[1];
    na = int((2*pi)/da) + 1;
    nb = int(pi/db) + 1;
    res1 = support(attracter,sphereUnit(1, 0, -pi/2))[1];
    lol = [res1[0] - bc[0], res1[1] - bc[1], res1[2] - bc[2]];
    res0 = sqrt(lol[0]*lol[0] + lol[1]*lol[1] + lol[2]*lol[2]);
    res = [res0, res1];
    for ia in range(0,na):
        for ib in range(0,nb):
            a = da*ia;
            b = -(pi/2) + db * ib;
            #print("looping though angles: ", (a/azerty), " : ", (b/azerty));
            s = support(attracter,sphereUnit(1, a, b))[1];
            w = [s[0] - bc[0],s[1] - bc[1],s[2] - bc[2]];
            length = sqrt(w[0]*w[0] + w[1]*w[1] + w[2]*w[2]);
            if(length > res[0]):
                res = [length, s];
    return res;

f = open("data/data0.3-0.001.txt", "r") #lire les données calculés.

print("loading file");
attracter = [];
for x in f:
  print(x);
  temp = x.split(" ");
  attracter+=[[float(temp[0]), float(temp[1]), float(temp[2])]];

print("file loaded! size: ", len(attracter));

bc = calculateBarycenter(attracter);
print("barycenter:");
print(bc);


radiusSupport = calculateRadius(attracter, [azerty*5, azerty*1], bc);
print("Radius support: ");
print(radiusSupport);

toMax = [radiusSupport[1][0] - bc[0],radiusSupport[1][1] - bc[1],radiusSupport[1][2] - bc[2]];
print("to max: ");
print(toMax);

extRadius = sqrt(toMax[0]*toMax[0] + toMax[1]*toMax[1] + toMax[2]*toMax[2]);
print("extRadius: ", extRadius);



fig2 = plt.figure()
ax = fig2.gca(projection='3d')
for i in range(0, len(attracter)):
    ax.scatter(attracter[i][0], attracter[i][1], attracter[i][2], c='pink', marker='*')
ax.scatter(bc[0], bc[1], bc[2], c='blue', marker='^', label="Baycentre",s=100);
ax.scatter(radiusSupport[1][0], radiusSupport[1][1], radiusSupport[1][2], c='red', marker='^', label="Radius Support",s=100);
ax.legend(loc="upper right")
ax.set_xlabel("u")
ax.set_ylabel("v")
ax.set_zlabel("w")
ax.set_title("KS")

plt.tight_layout()
plt.show()
