import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from mpl_toolkits.mplot3d import Axes3D
import warnings

def KS(y, t, params):
    u, v, w = y # Unpacking des variables
    E, c = params # Unpacking des parametres
    primes = [v, w, c*u - 0.5 * u**2  - v - E] # Calcul du flot
    return primes # On renvoie le flot

def onIntegre(positionInitiale, t, params):
    return odeint(KS, positionInitiale, t, args=(params,));

def converge(positionInitiale, t, params):
    print("testing");
    with warnings.catch_warnings():
        warnings.filterwarnings('error')
        try:
            sol = odeint(KS, positionInitiale, t, args=(params,))
            lastx = sol[len(sol)-1][0];
            lasty = sol[len(sol)-1][1];
            lastz = sol[len(sol)-1][2];
            length = lastx*lastx + lasty*lasty + lastz*lastz;
            if(length > 2):
                return False;
        except Warning as e:
            print("DIVERGENCE");
            return False;
    return True;

def calculerAttracteur(x, y, z, d, t, params):
    xstart = x[0];
    xend = x[1];
    ystart = y[0];
    yend = y[1];
    zstart = z[0];
    zend = z[1];
    dx = d[0];
    dy = d[1];
    dz = d[2];
    nx = int((xend - xstart)/dx) + 1;
    ny = int((yend - ystart)/dy) + 1;
    nz = int((zend - zstart)/dz) + 1;
    res = [];

    for i in range(0, nx):
        x = xstart + i*dx;
        for j in range(0, ny):
            y = ystart + j*dy;
            for k in range(0,nz):
                z = zstart + k*dz;
                print("x: ", x, " y: ", y, "z: ",z);
                if(converge([x,y,z], t, params)):
                    res += [[x,y,z]];
    print(res);
    return res;


# On definit les valeurs des parametres du systeme et les valeurs initiales
E = 0.001
c = 0.35

u0 = 0.2
v0 = -0.2
w0 = 0.001

# On pack tout pour les passer au solveur ensuite
params = [E, c]
y0 = [u0, v0, w0]

# Construction de l'axe du temps
T = 80 # Intervalle d'integration
dt = 0.1 # Pas d'integration
t = np.arange(0., T, dt)

# On fait appelle au solveur
sol = calculerAttracteur([-1,1], [-1,1], [-1,1], [0.05,0.05,0.05], t, params);
print("Saving positions...")
file = open("data/data" + str(c) +"-"+ str(E) +  ".txt","w+");
for i in range(0, len(sol)):
    file.write(str(sol[i][0]) + " " + str(sol[i][1]) + " " + str(sol[i][2]) + "\n");
file.close();

# On trace le resultat

legend = 'E=' + str(E) + '  c=' + str(c)



fig2 = plt.figure()
ax = fig2.gca(projection='3d')
for i in range(0, len(sol)):
    ax.scatter(sol[i][0], sol[i][1], sol[i][2], c='pink', marker='*')
ax.scatter(c, 0, 0, c='blue', marker='^', label="$\lambda_0$")
ax.scatter(-np.sqrt(abs(c**2-2*E)), 0.0, 0.0, c='red', marker='^', label="$\lambda-$")
ax.scatter(np.sqrt(abs(c**2-2*E)), 0, 0, c='green', marker='^', label="$\lambda+$")
ax.scatter(y0[0], y0[1], y0[2], c='pink', marker='^', label='$x_0$')
ax.legend(loc="upper right")
ax.set_xlabel("u")
ax.set_ylabel("v")
ax.set_zlabel("w")
ax.set_title("KS")

plt.tight_layout()
plt.show()
