\documentclass{article}
    % General document formatting
   \usepackage[margin=0.8in]{geometry}
    \usepackage[parfill]{parskip}
    \usepackage[utf8]{inputenc}
    \usepackage{graphicx}
    \usepackage{hyperref}
     \usepackage{float} 
     \usepackage{listings}
     \usepackage{color}
\usepackage[table]{xcolor}
    \definecolor{darkWhite}{rgb}{0.94,0.94,0.94}
    % Related to math
    \usepackage{amsmath,amssymb,amsfonts,amsthm}
\setlength\parindent{24pt}

\lstset{
  aboveskip=3mm,
  belowskip=-2mm,
  backgroundcolor=\color{darkWhite},
  basicstyle=\footnotesize,
  breakatwhitespace=false,
  breaklines=true,
  captionpos=b,
  commentstyle=\color{red},
  deletekeywords={...},
  escapeinside={\%*}{*)},
  extendedchars=true,
  framexleftmargin=16pt,
  framextopmargin=3pt,
  framexbottommargin=6pt,
  frame=tb,
  keepspaces=true,
  keywordstyle=\color{blue},
  language=C,
  morekeywords={*,...},
  numbers=left,
  numbersep=10pt,
  numberstyle=\tiny\color{black},
  rulecolor=\color{black},
  showspaces=false,
  showstringspaces=false,
  showtabs=false,
  stepnumber=1,
  stringstyle=\color{gray},
  tabsize=4,
  title=\lstname,
}
\begin{document}

\title{Équation de Kuramoto–Sivashinsky : Analyse d'un système dynamique et approche numérique}
\author{Ivan Doubovik\\
Bertrand Lovery\\
Pierre Wulles\\
Université Grenoble-Alpes\\
   \texttt{pierrewulles@vivaldi.net}}
\date{\today}

\maketitle
\section{Introduction}
\subsection{Objectifs}
\indent \indent Le but de ce document est de donner une vision d'ensemble sur le comportement du potentiel dit de Kuramoto–Sivashinsky et de réaliser des simulations autour de l'équation pour avoir des informations précises sur les zones de stabilités  ou d'instabilités du système.\\

L'équation de Kuramoto–Sivashinsky (KS dans la suite) est une équation différentielle partielle d'ordre 4 non linéaire. Elle fut établie dans les années 70 par G.I.Sivashinsky qui l'utilisât pour décrire le comportement d'un front de flamme. C'est ce cas que nous étudierons ici en reprenant l'analyse de Sivashinsky dans les deux premières sections, nous la mettrons en perspective avec les simulations informatiques que nous pouvons aujourd'hui réaliser et avec les connaissances actuelles concernant l'étude des systèmes dynamiques.\\

Nous pourrons ainsi visualiser facilement dans les dernières sections le tore que forme la solution dans l'espace des phases et nous analyserons les cas de divergence, enfin nous étudierons succinctement un modèle proche qui résout le problème de la stabilité.
\subsection{Notations et définitions}
Dans tout le document $K$ désignera un compact de dimension finie sans bord munie d'une norme que l'on notera $||\cdot||_K$ et on notera $T$ tout Tore inclus dans $K$.\\
%On notera $K_3$ le compact de dimension 3 inclus dans $K$.\\
Pour une fonction $f$ de plusieurs variables on notera les dérivées partielles en mettant la variable en indice:
\[ \frac{\partial f}{\partial x} = f_x \qquad \frac{\partial f}{\partial y} = f_y \]
On supposera que les fonctions manipulées seront toutes lisses, c'est à dire au moins $\mathcal{C}^1$ pour toutes leurs variables et on sera donc en mesure d'appliquer le théorème de Schwarz:
\[ f_{xy} = f_{yx} \]
\section{Origine de l'équation}
\subsection{Hypothèses du modèle}
\indent \indent Le modèle établi par Sivashinsky s'applique au déplacement d'un front de flamme dans un gaz qui vérifie les hypothèses suivantes :
\begin{itemize}
\item Le gaz est bi-moléculaire
\item La réaction est exothermique
\item Le combustible est initialement dans des proportions stœchiométriques
\end{itemize}
Concernant la flamme en elle-même on effectuera les hypothèses suivantes :
\begin{itemize}
\item Le gaz est immobile
\item Le gaz ne se dilate pas sous l'effet de la chaleur
\item La densité du gaz est partout constante égale à $\rho_0$.
\end{itemize}
\subsection{L'équation due à Sivashinsky}
On peut alors écrire l'équation de diffusion pour les deux concentrations des composants du gaz que l'on nomment $C_1$ et $C_2$ et l'équation de diffusion de la chaleur pour la fonction Température $T$ :
\begin{align}
\frac{\partial C_i}{\partial t} + \frac{\partial C_i}{\partial x} &= \frac{1}{L_i} \nabla^2 C_i - W(C_1, C_2, T)\qquad i=1,2\\
\frac{\partial T}{\partial t} + \frac{\partial T}{\partial x} &= \nabla^2T + (1-\sigma)W(C_1, C_2, T)
\end{align}
Où $L_i$ et $\sigma$ représentent des constantes spécifiques à la réalisation de l'expérience et $W$ est le potentiel d'échange chimique :
\[ W = AE_0^3C_1C_2\exp\left(1-\frac{1}{T}\right) \]
Cette expression provient de la loi d'Arrhenius où $E_0$ représente une énergie d'activation et $A$ une constante dépendant des conditions initiales du problème. On introduit ensuite la vitesse de déplacement de la flamme $u$ et on obtient (pour le calcul complet voir \cite{ref0}) l'équation suivante pour le régime laminaire :
\begin{align}
\frac{\partial u}{\partial \tau} = -u\nabla u - \nabla^2 u - \nabla^4 u 
\end{align}
On se restreindra ici à une seule dimension :
\begin{align}
u_{\tau} = -uu_x - u_{xx} - u_{xxxx} 
\end{align}
Où on a utilisé une notation plus compacte pour désigner les dérivées partielles. \\
On notera que cette équation vérifie des conditions au bord, c'est à dire qu'il existe $L$ tel que $u(x+L,t) = u(x,t)$, on peut donc s'attendre à trouver des solutions périodiques.\\
Cette équation est l'équation de Kuramoto–Sivashinsky à une dimension, elle sera notre objet d'étude dans les parties suivantes.

\newpage
\section{Analyse de la dynamique de l'équation}
Pour étudier l'équation nous allons effectuer le changement de variable $\epsilon = x - c\tau$ afin de n'avoir que des dérivées selon $\epsilon$ :
\begin{align*}
\frac{\partial \epsilon}{\partial \tau} = -c\qquad \frac{\partial \epsilon}{\partial x} = 1
\end{align*}
Cette transformation est légitime car l'équation est invariante par transformation de Galilée:
\begin{align}
-cu_{\epsilon} = -uu_{\epsilon} -u_{\epsilon\epsilon} - u_{\epsilon\epsilon\epsilon\epsilon}
\end{align}
Il suffit alors d'intégrer par rapport à $\epsilon$ pour obtenir l'équation :
\begin{align}
cu = \frac{1}{2} u^2 + u_{\epsilon} + u_{\epsilon\epsilon\epsilon} + E
\end{align}
Où $E$ a la dimension d'une énergie et $E>0$.\\ 
Cette équation, bien que sous une forme plus adaptée, ne peut être étudiée en tant que telle : il faut se ramener à une équation du premier degré, on introduit pour cela deux nouvelles fonctions $v$ et $w$ telles que $v = \dot{u}$ et $w = \dot{v}$. Le flot $\phi_{KS}$ vérifie  alors l'équation :
\begin{align}
\left\{ \begin{array}{ll}
\dot{u} =& v\\
\dot{v} =& w\\
\dot{w} =& cu - \frac{1}{2}u^2 - v -E 
\end{array}\right.
\end{align}
On remarque que ce flot est conservatif car $\mathrm{div}(\phi_{KS}) = O$ 
On va alors pouvoir chercher les points fixes de ce système :
\begin{align}
\left\{ \begin{array}{ll}
 v = 0\\
 w = 0\\
cu - \frac{1}{2}u^2 - v -E = 0 
\end{array}\right. \iff \left\{ \begin{array}{ll}
 v = 0\\
 w = 0\\
cu - \frac{1}{2}u^2 -E = 0 
\end{array}\right.
\end{align}
On pose $\Delta = c^2 -2E$, si $\Delta = 0$ alors on a un seul point fixe :
\[
x^*_0 = \begin{pmatrix}
0\\
0\\
c
\end{pmatrix}
\]
et sinon on a deux points fixes :
\[
x^*_1 = \begin{pmatrix}
0\\
0\\
c - \sqrt{c^2 - 2E}
\end{pmatrix}\qquad x^*_2 = \begin{pmatrix}
0\\
0\\
c + \sqrt{c^2 - 2E}
\end{pmatrix}
\]
 On va maintenant étudier la stabilité des points fixes en calculant la matrice Jacobienne du système :
\begin{align}
J = \begin{pmatrix}
1 & 0 & 0 \\
0 & 1 & 0 \\
-1 & 0 & c - u
\end{pmatrix}
\end{align}
Rappelons dès maintenant un théorème fondamental en systèmes dynamiques :\\

\textbf{Théorème:} Un point d'équilibre $x^*$ d'une équation différentielle est stable si toutes les valeurs propres de la matrice Jacobienne évaluée au point $x^*$ ont leurs parties réelles négatives. L'équilibre est instable si au moins une valeur propre a sa partie réelle positive.\\

\noindent Pour $x^*_0$, en reprenant l'expression $(9)$ :
\[J_{x^*_0} = \begin{pmatrix}
1 & 0 & 0 \\
0 & 1 & 0 \\
-1 & 0 & 0
\end{pmatrix} \]
On détermine les valeurs propres en calculant le déterminant :
\[ | J_{x^*_0} - \lambda I_3 | = -\lambda(1-\lambda)^2\]
Donc : 
\[ \mathrm{Sp}( J_{x^*_0} ) = \{0,1\} \]
Ce point fixe est donc clairement instable. Occupons nous maintenant de $x^*_1$ et $x^*_2$ :
\[J_{x^*_{1,2}} = \begin{pmatrix}
1 & 0 & 0 \\
0 & 1 & 0 \\
-1 & 0 & \pm \sqrt{c^2 - 2E}
\end{pmatrix} \]
Et donc :
\[ \mathrm{Sp}( J_{x^*_{1,2}} ) = \{1, \pm\sqrt{c^2-2E}\} \]
On a donc affaire soit à un point selle, soit à un point d'équilibre instable.
%Stabilisation des solutions si ajout du terme de benney ? yes ça marche du tonnerre !
\section{Simulations numérique}

\indent\indent Dans cette section nous allons réaliser des simulations pour déterminer point par point les valeurs des fonctions $u$, $v$ et $w$. On suppose que l'espace des phases est inclus dans $K$, ici de dimension trois. Nous allons voir que les trajectoires s'organisent autour d'un attracteur étrange de forme torique ou divergent en temps fini.\\

Pour réaliser les simulations numériques nous allons utiliser \texttt{python} et la bibliothèque \texttt{scipy}. L'algorithme sous-jacent utilisé est celui de Runge-Kutta d'ordre 4.\\

Nous allons fixer l'énergie de façon arbitraire $E = 0.001$ et nous ferons varier le paramètre $c$. Sauf exception nous utiliserons la condition initiale $x_0$:
\[ x_0 = \left| \begin{array}{ll}
u_0 &= 0.1\\
v_0 &= 0\\
w_0 &= 0
\end{array}\right.\]
Cette condition initiale ne se situe pas trop prêt du point fixe répulsif afin de pouvoir observer des phénomènes autre qu'une divergence systématique.\\

Tous les codes sont disponibles en Annexe et sur ce dépôt \href{https://gitlab.com/starcluster/report-on-kuramoto--sivashinsky-equation}{\texttt{GitLab}}.
\subsection{Explosion des solutions en temps fini}
\indent \indent Nous allons donner un sens précis à l'expression "exploser en temps fini": On dira de la solution $x$ d'une équation différentielle que l'on étudie sur $K$ où elle vérifie des conditions de régularité suffisantes  qu'elle explose en un temps fini $\Omega$ si :
\begin{align}
\forall M > 0, \exists \varepsilon > 0, \forall t, |t-\Omega| < \varepsilon, t\ne\Omega \Rightarrow || x(t) ||_K > M
\end{align}
Pour l'équation de KS il est possible de déterminer avec précision le temps d'explosion théorique (cf \cite{ref1}).
\[ \Omega \propto \ln \left| \frac{u_0 - 1}{u_0 + 1}\right| \]

\newpage
En lançant la simulation on obtient donc pour $c=0.1$ :

\[\includegraphics[scale=0.23]{explosion_en_temps_fini.png}\includegraphics[scale=0.3]{explosion_en_temps_fini_3D.png}\]

On observe donc un début périodique qui finit par diverger complètement, on peut synthétiser les trois graphes précédents en une seule représentation dans l'espace des phases de dimension 3, voir Figure
de droite. Dès que la solution s'approche du point d'équilibre négatif la solution explose.

On peut alors plus clairement noter que la solution oscille quelques temps autour des points d'équilibre avant de s'éloigner complètement pour ne jamais revenir. Approcher le point d'équilibre entraîne une explosion des solutions, si on place par exemple le point $x_0$ sur le point d'équilibre le résultat est immédiat :
\[\includegraphics[scale=0.23]{div_immediat}\includegraphics[scale=0.4]{div_immediat_3D.png}\]
Dans la partie suivante nous allons étudier des valeurs de $c$ et $x_0$ pour lesquelles on n'a pas de divergence.
\subsection{Stabilisation de la solution en cycle périodique}
Si l'on choisit $c=0.1$ et une condition initale la solution s'enroule autour d'un tore dans l'espace des phases, cet attracteur a pour centre le point d'équilibre positif, voir Figure 3 et 4. On peut remarquer si on laisse tourner la simulation suffisamment longtemps et si l'on choisit des conditions initiales telles que $u_0/v_0 \in \mathbb{Q}$ que l'espace des phases est dense dans le tore attracteur, c'est à dire que le théorème de Kronecker-Weyl est vérifié pour une solution $x$ :
\begin{align}
\underset{T \to \infty}{\lim} \int_0^T x(t)\mathrm{d}t = \frac{1}{\mathrm{Vol}(\mathbb{T})} \int_\mathbb{T} x(q)\mathrm{d}q
\end{align}

\[\includegraphics[scale=0.25]{attracteur1.png}\includegraphics[scale=0.22]{attracteur1_3D.png}\]
On peut obtenir pour certaines conditions initiales et valeurs de $c$, $c=0.2=u_0$, des trajectoires plus originales :
\[\includegraphics[scale=0.45]{chaos_3D.png}\]
On peut aussi obtenir une belle visualisation du Tore pour $c=0.25$, $u_0=0.3$, $v_0 = -0.4$ et $w=0$, le point initial est indiqué en rose :
\[\includegraphics[scale=0.45]{tore_3D.png}\]
Après analyse de plusieurs conditions initiales il est possible d'émettre la conjecture suivante : \\

\textbf{Conjecture} : Au delà d'une certaine valeur de $c$, tout point initial qui se situe à l'intérieur du tore attracteur $\mathbb{T}$ aboutit sur une trajectoire stable qui reste à l'intérieur du Tore.\\

%ajouter un exemple flagrant de trajectoire chaotique !
Dans la partie suivante nous allons caractériser précisément ce Tore.
\subsection{Caractérisation du Tore attracteur}
En terme de mathématiques, nous n'avons pas pu caractériser le tore de manière analytique. Ainsi, dans cette partie, nous avons recours à l'outil informatique. En exploitant le fait que si les conditions initiales se trouvent dans notre attracteur, alors l'évolution du système restera dans cet attracteur et si les conditions initiales ne sont pas dans l'attracteur alors notre système diverge.

Pour interpréter ceci, nous avons écrit deux algorithmes, le premier calculant l'attracteur en passant sur tous les points contenus dans un volume cubique, par incrémentation. Ce calcul étant très long, les données sont ensuite sauvegardées dans un fichier texte prêtes à être exploitées. Le deuxième algorithme sert à exploiter les données, il permet de calculer le barycentre de l'attracteur ainsi qu'une approximation du rayon extérieur, sachant que l'attracteur est de type Tore.

Pour calculer cette approximation du rayon extérieur, nous avons, selon plusieurs directions, effectué le produit scalaire entre la position du point relative au barycentre et le vecteur unitaire. Nous avons ainsi conservé le point ayant le plus important face au produit scalaire, ce qui nous permet d'obtenir la valeur du rayon extérieur.

En calculant pour différentes valeurs de paramètres, on en déduit que la première coordonnée du barycentre est environ égale au paramètre $C$. Le rayon du tore augmente avec $C$.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.5]{image_analyseur.png}
    \caption{Caption}
    \label{fig: Diagramme obtenu avec le programme analyseur pour 0.35}
\end{figure}

\newpage
\section{Au delà du modèle de Kuramoto-Sivashinsky}
\indent \indent Si l'on ajoute un terme en dérivée seconde au système de KS, c'est à dire que l'on modifie le flot en un nouveau flot $\phi_{KSB}$:
\begin{align}
\left\{ \begin{array}{ll}
\dot{u} =& v\\
\dot{v} =& w\\
\dot{w} =& cu - \frac{1}{2}u^2 - v - E  - \gamma w
\end{array}\right.
\end{align}
Alors le flot n'est plus conservatif car on a cette fois $\mathrm{div}(\phi_{KS}) = - \gamma < 0$ et on observe immédiatement que les trajectoires n'oscillent plus en un tore mais convergent rapidement soit sur un cycle limite soit sur une solution stationnaire, on appelle ce modèle Kuramoto-Sivashinsky-Benney, on peut en trouver une étude plus complète ici \cite{ref1}.
\[\includegraphics[scale=0.25]{KSB1.png}\includegraphics[scale=0.35]{KSB1_3D.png}\]
 
\section{Conclusion}
\indent \indent L'étude de l'équation de KS nous a permis de découvrir que ce système est hautement instable, les cas de stabilité sont ceux où les trajectoires dans l'espace des phases s'enroulent autour d'un Tore que nous avons caractérisé, ces cas correspondent à certaines valeurs de $c$ et quand les conditions initiales se situent à l'intérieur du Tore. On a également mis en évidence des trajectoires chaotiques dès que l'on s'approche du point fixe répulsif.
\large

\newpage
\section*{Annexe}
Code utilisé pour générer les figures :
\lstinputlisting[language=Python, firstline=0, lastline=77]{code.py}

\newpage
\nocite{ref2}
\nocite{ref3}

\bibliographystyle{plain}

\bibliography{biblio}

\end{document}
