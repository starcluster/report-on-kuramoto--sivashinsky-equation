import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from mpl_toolkits.mplot3d import Axes3D
import warnings

def KS(y, t, params):
    u, v, w = y # Unpacking des variables
    E, c = params # Unpacking des parametres
    primes = [v, w, c*u - 0.5 * u**2  - v - E] # Calcul du flot
    return primes # On renvoie le flot

# On definit les valeurs des parametres du systeme et les valeurs initiales
E = 0.001
#c = 0.2
c = 0.6
u0 = 0.2
v0 = -0.2
w0 = 0.001

# On pack tout pour les passer au solveur ensuite
params = [E, c]
y0 = [u0, v0, w0]

# Construction de l'axe du temps
T = 80 # Intervalle d'integration
dt = 0.1 # Pas d'integration
t = np.arange(0., T, dt)

# On fait appelle au solveur avec gestion de l'erreur si divergence (tres utile pour automatiser detection des divergences
with warnings.catch_warnings():
    warnings.filterwarnings('error')
    try:
        sol = odeint(KS, y0, t, args=(params,))
    except Warning as e:
        print("DIVERGENCE")

# On trace le resultat
fig = plt.figure(1, figsize=(10,10))

legend = 'E=' + str(E) + '  c=' + str(c)

ax1 = fig.add_subplot(411)
ax1.plot(t, sol[:,0], 'r', label=legend)
ax1.legend(loc='upper right')
ax1.set_xlabel('t')
ax1.set_ylabel('u')

ax2 = fig.add_subplot(412)
ax2.plot(t, sol[:,1], 'b', label=legend)
ax2.legend(loc='upper right')
ax2.set_xlabel('t')
ax2.set_ylabel('v')

ax3 = fig.add_subplot(413)
ax3.plot(t, sol[:,2], 'g', label=legend)
ax3.legend(loc='upper right')
ax3.set_xlabel('t')
ax3.set_ylabel('w')

ax4 = fig.add_subplot(414)
ax4.plot(sol[:,1], sol[:,2], '-', ms=1, label=legend)
ax4.legend(loc='upper right')
ax4.set_xlabel('v')
ax4.set_ylabel('w')

fig2 = plt.figure()

ax = fig2.gca(projection='3d')
ax.plot(sol[:,0], sol[:,1], sol[:,2], lw=0.5)
ax.scatter(c, 0, 0, c='blue', marker='^', label="$\lambda_0$")
ax.scatter(-np.sqrt(abs(c**2-2*E)), 0.0, 0.0, c='red', marker='^', label="$\lambda-$")
ax.scatter(np.sqrt(abs(c**2-2*E)), 0, 0, c='green', marker='^', label="$\lambda+$")
ax.scatter(y0[0], y0[1], y0[2], c='pink', marker='^', label='$x_0$')
ax.legend(loc="upper right")
ax.set_xlabel("u")                                                                                
ax.set_ylabel("v")                                                                                 
ax.set_zlabel("w")                                                                                 
ax.set_title("KS") 
    
plt.tight_layout()
plt.show()
